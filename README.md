# Pisces System Plymouth Theme

Pisces System boot splash.

## Build & Install (To be tested|For Ubuntu/Debian)

```shell
sudo mk-build-deps ./debian/control -i -t "apt-get --yes" -r
dpkg-buildpackage -b -uc -us
```

## License

This project has been licensed by GPLv3.
Piscesys Logo has been licensed by CC-BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0/>.
